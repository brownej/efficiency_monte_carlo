extern crate getopts;
extern crate rand;
extern crate rayon;

pub mod angle;
pub mod interpolation;
pub mod kinematics;
pub mod simulation;

use angle::AngleInfo;
use getopts::Options;
use kinematics::KinematicLine;
use rayon::prelude::*;
use simulation::{Simulation, Hit};
use std::env;
use std::fs::File;
use std::io::Write;
use std::path::Path;
use std::sync::Mutex;

fn main() {
    let mut opts = Options::new();
    opts.long_only(true);
    opts.optflag("h", "help", "Show this help message.");
    opts.optopt(
        "n",
        "",
        "Number of reactions to simulate (default: 1000).",
        "NUM",
    );
    opts.optflagopt(
        "",
        "34s",
        "Calculate efficiency of 34S(a,p). If FILE is present, output the hits that were detected to FILE (Requires an `=`).",
        "FILE",
    );
    opts.optflagopt(
        "",
        "34cl",
        "Calculate efficiency of 34Cl(a,p). If FILE is present, output the hits that were detected to FILE (Requires an `=`).",
        "FILE",
    );
    opts.optflagopt(
        "",
        "34ar",
        "Calculate efficiency of 34Ar(a,p). If FILE is present, output the hits that were detected to FILE (Requires an `=`).",
        "FILE",
    );
    let args: Vec<String> = env::args().collect();
    let matches = opts.parse(&args[1..]).unwrap();

    if matches.opt_present("h") ||
        !matches.opts_present(&["34s".into(), "34cl".into(), "34ar".into()])
    {
        let prog = Path::new(&args[0]).file_name().unwrap().to_str().unwrap();
        println!("{}", opts.usage(&opts.short_usage(prog)));
        ::std::process::exit(0);
    }

    let num_in: u32 = matches
        .opt_str("n")
        .map(|x| {
            x.parse::<u32>().expect(
                "Argument for 'n' is cannot be parsed to a u32.",
            )
        })
        .unwrap_or(1000);

    let mut reactions = vec![];
    if matches.opt_present("34s") {
        let xsecs = get_xsecs(include_str!("data/34S_a_p__55.44MeV.out"));
        reactions.push((
            "34S(a,p)".to_string(),
            get_kin_lines(include_str!("data/34S.txt")),
            matches.opt_str("34s"),
            xsecs,
        ));
    }
    if matches.opt_present("34cl") {
        let xsecs = get_xsecs(include_str!("data/34Cl_a_p__55.44MeV.out"));
        reactions.push((
            "34Cl(a,p)".to_string(),
            get_kin_lines(include_str!("data/34Cl.txt")),
            matches.opt_str("34cl"),
            xsecs,
        ));
    }
    if matches.opt_present("34ar") {
        let xsecs = get_xsecs(include_str!("data/34Ar_a_p__55.44MeV.out"));
        reactions.push((
            "34Ar(a,p)".to_string(),
            get_kin_lines(include_str!("data/34Ar.txt")),
            matches.opt_str("34ar"),
            xsecs,
        ));
    }

    let sidar_angles = (
        "SIDAR",
        get_angles(include_str!("data/det_angles_sidar.cfg")),
    );
    let sorr_dnt_angles = (
        "SORR_DNT",
        get_angles(include_str!("data/det_angles_sorr_dnt.cfg")),
    );
    let sorr_up_angles = (
        "SORR_UP",
        get_angles(include_str!("data/det_angles_sorr_up.cfg")),
    );

    println!("==============================");
    println!("Simulation run with n = {}", num_in);
    for (reac_name, lines, f_name, xsecs) in reactions {
        let mut sim = Simulation::new(lines, xsecs).unwrap();
        sim.run(num_in);

        let hits = sim.results.into_inner().unwrap();

        let all_angles = vec![(&sidar_angles, Mutex::new(0u32)),
            (&sorr_dnt_angles, Mutex::new(0u32)),
            (&sorr_up_angles, Mutex::new(0u32))];

        let f = f_name.as_ref().map(|n| Mutex::new(File::create(n).unwrap()));
        hits.par_iter().for_each(|h| {
            let mut detected = false;
            for &(&(_, ref angles), ref num) in &all_angles {
                if gates(h, angles) {
                    *num.lock().unwrap() += 1u32;
                    detected = true;
                }
            }
            if let &Some(ref f) = &f {
                let detected = if detected { 1 } else { 0 };
                writeln!(
                    f.lock().unwrap(),
                    "{} {} {} {} {} {} {}",
                    h.ejectile_theta,
                    h.ejectile_phi,
                    h.ejectile_energy,
                    h.recoil_theta,
                    h.recoil_phi,
                    h.recoil_energy,
                    detected
                ).unwrap();
            }
        });

        println!("");
        for (&(angles_name, _), num) in all_angles {
            let num = num.into_inner().unwrap();
            let eff = (num as f64) / (hits.len() as f64);
            println!("{} {} Efficiency: {}", reac_name, angles_name, eff);
        }
    }
    println!("==============================");
}

fn gates(h: &Hit, angles: &[AngleInfo]) -> bool {
    if h.recoil_theta < 0.6 {
        return false;
    }
    if h.ejectile_energy < 0.5000 {
        return false;
    }
    for a in angles {
        if (h.ejectile_theta > a.theta_min && h.ejectile_theta < a.theta_max) &&
            (h.ejectile_phi > a.phi_min && h.ejectile_phi < a.phi_max)
        {
            return true;
        }
    }
    false
}

fn get_kin_lines(s: &str) -> Vec<KinematicLine> {
    let mut kin_lines = Vec::new();

    let mut com_angles = Vec::new();
    let mut ejectile_angles = Vec::new();
    let mut ejectile_energies = Vec::new();
    let mut recoil_angles = Vec::new();
    let mut recoil_energies = Vec::new();
    let mut weights = Vec::new();

    for l in s.lines() {
        let x: Vec<_> = l.split_whitespace().collect();

        if x.is_empty() && !com_angles.is_empty() {
            // If there's an empty line, we're starting a new kinematic line
            // Don't add a new kinematic line if it's empty
            kin_lines.push(
                KinematicLine::new(
                    com_angles,
                    ejectile_angles,
                    ejectile_energies,
                    recoil_angles,
                    recoil_energies,
                    weights,
                ).unwrap(),
            );
            com_angles = Vec::new();
            ejectile_angles = Vec::new();
            ejectile_energies = Vec::new();
            recoil_angles = Vec::new();
            recoil_energies = Vec::new();
            weights = Vec::new();
        } else if x[0].starts_with("#") {
            // Ignore comments
            continue;
        } else {
            // Build current kinematic line
            com_angles.push(x[0].parse::<f64>().unwrap());
            ejectile_angles.push(x[1].parse::<f64>().unwrap());
            ejectile_energies.push(x[2].parse::<f64>().unwrap());
            recoil_angles.push(x[4].parse::<f64>().unwrap());
            recoil_energies.push(x[5].parse::<f64>().unwrap());
            weights.push(1.0);
        }
    }

    // If there's any left over data, add it as another kinematic line
    // This is important if the file doesn't end with a new line
    if !com_angles.is_empty() {
        kin_lines.push(
            KinematicLine::new(
                com_angles,
                ejectile_angles,
                ejectile_energies,
                recoil_angles,
                recoil_energies,
                weights,
            ).unwrap(),
        );
    }

    kin_lines
}

fn get_xsecs(s: &str) -> Vec<f64> {
    let mut xsecs = Vec::new();

    for l in s.lines() {
        let x: Vec<_> = l.split_whitespace().collect();
        if x.is_empty() || x[0].starts_with('#') {
            continue;
        } else {
            let xsec = x[6].parse::<f64>().unwrap();

            xsecs.push(xsec);
        }
    }

    xsecs
}

fn get_angles(s: &str) -> Vec<AngleInfo> {
    let mut angles = Vec::new();

    for l in s.lines() {
        let x: Vec<_> = l.split_whitespace().collect();
        if x.is_empty() || x[0].starts_with('#') {
            continue;
        } else if x.len() < 8 {
            eprintln!("WARNING: Error parsing a line in the angle file.");
        } else {
            let theta_min = x[2].parse::<f64>().unwrap();
            let theta_max = x[3].parse::<f64>().unwrap();
            let theta_avg = x[4].parse::<f64>().unwrap();

            let phi_min = x[5].parse::<f64>().unwrap();
            let phi_max = x[6].parse::<f64>().unwrap();
            let phi_avg = x[7].parse::<f64>().unwrap();

            angles.push(AngleInfo {
                theta_min,
                theta_max,
                theta_avg,
                phi_min,
                phi_max,
                phi_avg,
            });
        }
    }

    angles
}
