use rayon::prelude::*;
use std::sync::Mutex;
use kinematics::KinematicLine;
use rand::thread_rng;
use rand::distributions::{Range, IndependentSample, Weighted, WeightedChoice};

#[derive(Debug)]
pub struct Hit {
    pub ejectile_theta: f64,
    pub ejectile_phi: f64,
    pub ejectile_energy: f64,
    pub recoil_theta: f64,
    pub recoil_phi: f64,
    pub recoil_energy: f64,
}

pub struct Simulation {
    pub results: Mutex<Vec<Hit>>,
    kin_lines: Vec<KinematicLine>,
    xsecs: Vec<f64>,
}

impl Simulation {
    pub fn new(kin_lines: Vec<KinematicLine>, xsecs: Vec<f64>) -> Option<Simulation> {
        if kin_lines.len() != xsecs.len() {
            None
        } else {
            Some(Simulation {
                kin_lines,
                xsecs,
                results: Mutex::new(Vec::new()),
            })
        }
    }

    pub fn run(&mut self, n: u32) {
        let mut items = Vec::new();
        let weight_sum: f64 = self.xsecs.iter().sum();
        for (w, i) in self.xsecs.iter().zip(self.kin_lines.iter()) {
            let w_u32 = f64::floor(w / weight_sum * (::std::u32::MAX as f64) as f64) as u32;
            items.push(Weighted {
                weight: w_u32,
                item: i,
            });
        }
        let wc = WeightedChoice::new(&mut items);
        (0..n).into_par_iter().for_each(|_| {
            let hit = self.get_hit(&wc);
            self.results.lock().unwrap().push(hit);
        });
    }

    fn get_hit<'a>(&self, wc: &WeightedChoice<'a, &KinematicLine>) -> Hit {
        let mut rng = thread_rng();
        let line = wc.ind_sample(&mut rng);

        // TODO: Weight the angles based on something else?
        // This weights it based solely on solid angle
        // For a PDF(x) = sin(x) from 0 to pi,
        // the CDF(x) = 0.5 * (1 - cos(x))
        // Using the inverse of that, you can gen random numbers
        let rand_num = Range::new(0.0, 1.0).ind_sample(&mut rng);
        let com_theta = f64::acos(1.0 - 2.0 * rand_num).to_degrees();
        let phi = Range::new(0.0, 360.0).ind_sample(&mut rng);

        Hit {
            ejectile_theta: line.ejectile_theta(com_theta).to_value().unwrap(),
            ejectile_phi: phi,
            ejectile_energy: line.ejectile_energy(com_theta).to_value().unwrap(),
            recoil_theta: line.recoil_theta(com_theta).to_value().unwrap(),
            recoil_phi: if phi > 180.0 {
                phi - 180.0
            } else {
                phi + 180.0
            },
            recoil_energy: line.recoil_energy(com_theta).to_value().unwrap(),
        }
    }
}
