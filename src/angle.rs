#[derive(Debug, Clone)]
pub struct AngleInfo {
    pub theta_min: f64,
    pub theta_max: f64,
    pub theta_avg: f64,
    pub phi_min: f64,
    pub phi_max: f64,
    pub phi_avg: f64,
}
